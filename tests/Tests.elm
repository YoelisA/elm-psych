module Tests exposing (..)

import Canopy exposing (..)
import Expect
import Set exposing (..)
import Test exposing (..)
import TreeAutomaton exposing (automaton)



-- Check out https://package.elm-lang.org/packages/elm-explorations/test/latest to learn more about testing in Elm!


type alias Data =
    { id : String, require : String, provide : String }


automaton : Test
automaton =
    let
        anotherTree : Node Data
        anotherTree =
            node { id = "root", provide = "", require = "" } []
    in
    describe "A test suit for the automaton"
        [ test "A tree with a single root should produce a default automaton"
            (\_ ->
                Expect.equal
                    { nodeToVisit = []
                    , achievements = Set.fromList [ "human" ]
                    , visitedNodes = []
                    , tree = Node { id = "root", require = "", provide = "" } []
                    }
                    (TreeAutomaton.automaton anotherTree)
            )
        ]
