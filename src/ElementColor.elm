module ElementColor exposing (..)

import Element exposing (..)


lightGrey =
    Element.rgb 100 23 24
