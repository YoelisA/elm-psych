module TreeAutomaton exposing (..)

import Canopy exposing (..)
import Set exposing (..)


default : Default
default =
    { id = "somthng", require = "", provide = "", show = ( "", 0 ) }


type alias HasId a =
    { a | id : String }


type alias Default =
    HasId Data


type alias Data =
    { id : String
    , require : String
    , provide : String
    , show : ( String, Int )
    }


automaton : Node Data -> Memory
automaton tree =
    Canopy.foldl
        (\block auto ->
            case Set.member block.require auto.achievements of
                True ->
                    case List.member block.id auto.visitedNodes of
                        True ->
                            auto

                        False ->
                            if auto.innerTimeline >= Tuple.second block.show then
                                { auto
                                    | innerTimeline = auto.innerTimeline + 1
                                    , see = Tuple.first block.show
                                }

                            else
                                { auto | innerTimeline = 0, see = "" }

                False ->
                    auto
        )
        initMemory
        tree


type alias Memory =
    { nodeToVisit : List String
    , achievements : Set.Set String
    , visitedNodes : List String
    , tree : Node Data
    , innerTimeline : Int
    , see : String
    }


initMemory : Memory
initMemory =
    { nodeToVisit = []
    , achievements = Set.fromList [ "human" ]
    , visitedNodes = []
    , tree = node { id = "root", require = "", provide = "", show = ( "", 0 ) } []
    , innerTimeline = 0
    , see = ""
    }


type Automaton
    = Automaton Memory NextMovement


type State
    = VisitingNode
    | Moving
    | NoMoreNodeToVisitl


type NextMovement
    = GoDown
    | GoUp
    | GoRight
    | GoLeft
