module Main exposing (..)

import Block exposing (Node, emptyNode, ms)
import Canopy exposing (node)
import Color exposing (..)
import Environnement
    exposing
        ( block
        , environnement
        , keyToAction
        , require
        , setId
        , setKeyboard
        , setScreen
        , show
        )
import Shape
    exposing
        ( moveDown
        , moveLeft
        , moveRight
        , moveUp
        , rectangle
        , rotate
        )


main =
    environnement
        [ setKeyboard
            [ keyToAction "ArrowLeft" "ToPrev"
            ]
        , setScreen 600 400
        ]
        [ block
            [ setId "Introduction"
            , require [ "woman", "correctScreen" ]
            ]
            [ show (rectangle blue 45 45) (ms 300) (ms 400)
            , show (rectangle red 60 45) (ms 400) (ms 500)
            ]
        ]
