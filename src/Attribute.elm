module Attribute exposing (Attribute(..))

import Computer exposing (..)
import Set exposing (Set)


type Attribute msg
    = NewScreen Screen
    | MappedSetOfKeys Mapping


type alias Mapping =
    Set ( String, String )
