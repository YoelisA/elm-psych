module Block exposing
    ( Achievement
    , Data
    , DataShape
    , Description
    , Expectation
    , Id
    , Ms
    , Node
    , Requirement
    , ScheduledPicture
    , emptyData
    , emptyNode
    , getChildren
    ,  ms
       --, toShape

    , newNodeDescription
    , newNodeId
    , newNodeRequirement
    , newSequence
    , schedulePicture
    )

import Canopy exposing (..)
import Shape exposing (..)


type alias Node =
    Canopy.Node Data


type Id
    = Id String


type Description
    = Description String


type Requirement
    = Requirement (List Achievement)


type Expectation
    = Expectation (List DataShape)


type alias Achievement =
    String


type alias DataShape =
    String


getChildren : Node -> List Node
getChildren (Node data children) =
    children


type alias Sequence =
    List Shape


type alias Data =
    { id : Id
    , description : Description
    , expectation : Expectation
    , require : Requirement
    , provide : List String
    , sequence : List ScheduledPicture
    , visited : Bool
    }


type ScheduledPicture
    = ScheduledPicture
        { startAt : Ms
        , endAt : Ms
        , picture : Shape
        }


schedulePicture : Shape -> Ms -> Ms -> ScheduledPicture
schedulePicture shape start end =
    ScheduledPicture { startAt = start, picture = shape, endAt = end }


type Ms
    = Ms Int


ms : Int -> Ms
ms int =
    Ms int


emptyNode =
    node emptyData []


emptyData : Data
emptyData =
    { id = setId ""
    , description = setDescription ""
    , expectation = setExpectation []
    , require = setRequirement []
    , provide = []
    , sequence = []
    , visited = False
    }


newSequence : ScheduledPicture -> Node -> Node
newSequence ((ScheduledPicture { startAt, endAt, picture }) as scheduledPicture) (Node prevData children) =
    leaf
        { prevData
            | sequence = scheduledPicture :: []
            , id =
                setId (whichForm picture)
        }


newNodeId : String -> Node -> Node
newNodeId id (Node prevData children) =
    Node
        { prevData
            | id = setId id
        }
        children


newNodeDescription : String -> Node -> Node
newNodeDescription description (Node prevData children) =
    Node
        { prevData
            | description = setDescription description
        }
        children


newNodeRequirement : List Achievement -> Node -> Node
newNodeRequirement achievements (Node prevData children) =
    Node { prevData | require = setRequirement achievements } children


setId : String -> Id
setId id =
    Id id


setDescription : String -> Description
setDescription description =
    Description description


setExpectation : List DataShape -> Expectation
setExpectation dataShapes =
    Expectation dataShapes


setRequirement : List Achievement -> Requirement
setRequirement achievements =
    Requirement achievements



{--addShapes : Shape -> Sequence
addShapes shape =
    Sequence shape


toShape : Sequence -> Shape
toShape (Sequence shape) =
    shape--}
