module Shape exposing
    ( Form(..)
    , Shape
    , circle
    , emptyShape
    , move
    , moveDown
    , moveLeft
    , moveRight
    , moveUp
    , rectangle
    , render
    , rotate
    , whichForm
    )

import Browser
import Browser.Events as E
import Color exposing (..)
import Computer exposing (..)
import Html exposing (Html)
import Svg exposing (Svg)
import TypedSvg exposing (svg)
import TypedSvg.Attributes as Attr
import TypedSvg.Types exposing (Length, Paint(..), Transform(..), px)


rectangle : Color -> Float -> Float -> Shape
rectangle color width height =
    Shape
        { x = 0.0
        , y = 0.0
        , angle = 0.0
        , scale = 1.0
        , alpha = 1.0
        }
        (Rectangle color width height)


emptyShape : Shape
emptyShape =
    Shape
        { x = 0.0
        , y = 0.0
        , angle = 0.0
        , scale = 1.0
        , alpha = 1.0
        }
        None


shapeToForm : Shape -> Form
shapeToForm (Shape attr form) =
    form


whichForm : Shape -> String
whichForm =
    formToString << shapeToForm


formToString : Form -> String
formToString form =
    case form of
        Circle _ _ ->
            "circle"

        Rectangle _ _ _ ->
            "rectangle"
        Group _ ->
            "groupOfShapes"

        None ->
            "emptyShape"


circle : Color -> Float -> Shape
circle color radius =
    Shape { x = 0.0, y = 0.0, angle = 0.0, scale = 1.0, alpha = 1.0 } (Circle color radius)


rotate : Float -> Shape -> Shape
rotate da (Shape { x, y, angle, scale, alpha } form) =
    Shape
        { x = x
        , y = y
        , angle = angle + da
        , scale = scale
        , alpha = alpha
        }
        form


move : Position -> Shape -> Shape
move newPosition (Shape { x, y, angle, scale, alpha } form) =
    Shape
        { x = x + Tuple.first newPosition
        , y = y + Tuple.second newPosition
        , angle = angle
        , scale = scale
        , alpha = alpha
        }
        form


moveLeft : Float -> Shape -> Shape
moveLeft offset (Shape { x, y, angle, scale, alpha } form) =
    Shape
        { x = x - offset
        , y = y
        , angle = angle
        , scale = scale
        , alpha = alpha
        }
        form


moveRight : Float -> Shape -> Shape
moveRight offset (Shape { x, y, angle, scale, alpha } form) =
    Shape
        { x = x + offset
        , y = y
        , angle = angle
        , scale = scale
        , alpha = alpha
        }
        form


moveUp : Float -> Shape -> Shape
moveUp offset (Shape { x, y, angle, scale, alpha } form) =
    Shape
        { x = x
        , y = y - offset
        , angle = angle
        , scale = scale
        , alpha = alpha
        }
        form


moveDown : Float -> Shape -> Shape
moveDown offset (Shape { x, y, angle, scale, alpha } form) =
    Shape
        { x = x
        , y = y + offset
        , angle = angle
        , scale = scale
        , alpha = alpha
        }
        form


render : Screen -> List Shape -> Html.Html msg
render screen shapes =
    svg
        [ Attr.viewBox screen.left screen.bottom screen.width screen.height
        , Attr.style ""
        ]
        (List.map renderShape shapes)


group : List Shape -> Shape
group shapes =
    Shape
        { x = 0
        , y = 0
        , angle = 0
        , scale = 1
        , alpha = 1
        }
        (Group shapes)


renderShape : Shape -> Svg msg
renderShape (Shape { x, y, angle, scale, alpha } form) =
    case form of
        Rectangle color width height ->
            renderRectangle
                { color = color
                , width = width
                , height = height
                , x = x
                , y = y
                , angle = angle
                , scale = scale
                , alpha = alpha
                }

        Circle color radius ->
            renderCircle
                { color = color
                , radius = radius
                , x = x
                , y = y
                , angle = angle
                , scale = scale
                , alpha = alpha
                }

        Group shapes ->
            TypedSvg.g [ Attr.transform [] ] []

        None ->
            renderCircle
                { color = white
                , radius = 0
                , x = 0
                , y = 0
                , angle = 0
                , scale = 0
                , alpha = 0
                }


renderTransform { x, y, angle, scale } =
    if angle == 0 then
        if scale == 1 then
            Attr.transform [ Translate x -y ]

        else
            Attr.transform [ Translate x -y, Scale scale scale ]

    else if scale == 1 then
        Attr.transform [ Translate x -y, Rotate -angle 0 0 ]

    else
        Attr.transform [ Translate x -y, Rotate -angle 0 0 ]


renderRectangle : RectangleAttributes -> Svg msg
renderRectangle rectangle_ =
    TypedSvg.rect
        [ Attr.width (px rectangle_.width)
        , Attr.height (px rectangle_.height)
        , Attr.fill (Paint rectangle_.color)
        , Attr.transform
            [ Rotate rectangle_.angle (rectangle_.x + rectangle_.width / 2) (rectangle_.y + rectangle_.height / 2)
            , Translate rectangle_.x rectangle_.y
            ]
        ]
        []


renderCircle : CircleAttributes -> Svg msg
renderCircle circle_ =
    TypedSvg.circle
        [ Attr.r (px circle_.radius)
        , Attr.fill (Paint circle_.color)
        ]
        []


type alias RectangleAttributes =
    { color : Color
    , width : Float
    , height : Float
    , x : Float
    , y : Float
    , angle : Float
    , scale : Float
    , alpha : Float
    }


type alias CircleAttributes =
    { color : Color
    , radius : Float
    , x : Float
    , y : Float
    , angle : Float
    , scale : Float
    , alpha : Float
    }


type alias ShapeAttributes =
    { color : Color
    , width : Float
    , height : Float
    , x : Float
    , y : Float
    , angle : Float
    , scale : Float
    , alpha : Float
    , radius : Float
    }


type Shape
    = Shape
        { x : Float
        , y : Float
        , angle : Float
        , scale : Float
        , alpha : Float
        }
        Form


type Form
    = Circle Color Float
    | Rectangle Color Float Float
    | Group (List Shape)
    | None
