module Environnement exposing
    ( block
    , environnement
    , keyToAction
    , require
    , setId
    , setKeyboard
    , setScreen
    , show
    
    )

import Attribute exposing (..)
import Block
    exposing
        ( Achievement
        , Data
        , DataShape
        , Description
        , Expectation
        , Id
        , Ms
          --      , toShape
        , Node
        , Requirement
        , ScheduledPicture
        , emptyData
        , emptyNode
        , getChildren
        , newNodeDescription
        , newNodeId
        , newNodeRequirement
        , newSequence
        , schedulePicture
        )
import Browser
import Browser.Events as E
import Canopy exposing (getAll)
import Color exposing (..)
import Computer exposing (..)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Input as Input
import ElementColor as Col
import Html exposing (Html, text)
import Json.Decode as D
import Set exposing (Set)
import Shape exposing (..)
import Time exposing (Posix)
import Tuple exposing (pair)



--todo = remove redondancy between nodeProperty and Attribute.Attribute


type alias Model =
    { computer : Computer
    , keyMapping : Set ( String, String )
    , visibility : E.Visibility
    , tree : Canopy.Node Data
    , playing : Bool
    , title : String
    , view : ViewType

    --
    --
    -- subjectData
    , nodesToVisit : Set String
    , achievements : Set String
    , visitedNodes : Set String
    , innerTimeline : Float
    , seeing : Shape
    }


type alias SubjectData a =
    { a
        | nodesToVisit : Set String
        , achievements : Set String
        , visitedNodes : Set String
        , innerTimeline : Float
        , seeing : Shape
        , tree : Canopy.Node Data
    }


type alias ExperimentTree a =
    { a
        | tree : Node
    }


type ViewType
    = Preview


environnement : List (Attribute.Attribute msg) -> List Node -> Program () Model Msg
environnement attributes children =
    let
        root =
            Canopy.node emptyData children

        {--pictures =
            root
                |> Canopy.leaves
                |> List.foldl
                    (\{ description, expectation, id, require, sequence } acc ->
                        sequence ++ acc
                    )
                    []
                --}
        initKeyMapping =
            List.foldl
                (\x acc ->
                    case x of
                        MappedSetOfKeys set ->
                            Set.union set acc

                        _ ->
                            acc
                )
                Set.empty
                attributes

        newScreen =
            List.foldl
                (\x acc ->
                    case x of
                        NewScreen { width, height } ->
                            toScreen width height

                        _ ->
                            acc
                )
                (toScreen 600 600)
                attributes

        init_ () =
            ( { computer = initComputer newScreen
              , keyMapping = initKeyMapping
              , visibility = E.Visible

              --  , thingsToShow = []
              , tree = root
              , playing = False
              , title = "Init experiment"
              , view = Preview

              --
              --
              --
              --InitSubject
              , nodesToVisit = Set.fromList []
              , achievements = Set.fromList []
              , visitedNodes = Set.fromList []
              , innerTimeline = 0
              , seeing = rectangle red 10 10
              }
            , Cmd.none
            )
    in
    Browser.document
        { init = init_
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


cross =
    [ rectangle red 30 10
    , rectangle red 10 30 |> Shape.moveRight 10 |> Shape.moveUp 10
    ]


view : Model -> { body : List (Html Msg), title : String }
view model =
    { title = model.title
    , body =
        [ layout [ Background.color (Element.rgb 10 100 10) ] <|
            column
                [ Element.width (px (round model.computer.screen.width))
                , Element.height (px (round model.computer.screen.height))
                , Border.width 10
                , centerX
                , centerY
                , Element.inFront (Element.el [] (viewOptions model))
                ]
                [ Element.html <|
                    render model.computer.screen [ rectangle red 10 10 ]
                ]
        ]
    }


viewOptions model =
    let
        viewStateButtonPlay =
            if model.playing then
                "Pause"

            else
                "Play"
    in
    Element.column
        [ Element.width (px (round model.computer.screen.width))
        , Element.height (px (round model.computer.screen.height))
        ]
        [ Input.button []
            { onPress = Just (OptionButtonPressed "pause")
            , label = Element.el [] (Element.text viewStateButtonPlay)
            }
        ]


subscriptions model =
    Sub.batch
        [ pausableAnimationFrame model.playing
        , E.onKeyUp (D.map (KeyChanged False) (D.field "key" D.string))
        , E.onKeyDown (D.map (KeyChanged True) (D.field "key" D.string))
        , E.onVisibilityChange VisibilityChanged
        ]


pausableAnimationFrame =
    \x ->
        case x of
            False ->
                Sub.none

            True ->
                E.onAnimationFrame Tick


updateSubject : Msg -> SubjectData a -> SubjectData a
updateSubject msg subject =
    case msg of
        Tick posixTime ->
            Canopy.foldl
                (\node subj ->
                    subj
                )
                { subject
                    | nodesToVisit = subject.nodesToVisit
                    , achievements = subject.achievements
                    , visitedNodes = subject.visitedNodes
                    , innerTimeline = subject.innerTimeline
                    , seeing = subject.seeing
                }
                subject.tree

        _ ->
            subject


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        KeyChanged isDown key ->
            ( { model
                | computer =
                    updateComputerKeyboard
                        { isDown = isDown
                        , key = key
                        , mappedSetOfKeys = model.keyMapping
                        }
                        model.computer.keyboard
                        model.computer
              }
            , Cmd.none
            )

        Tick time ->
            ( model, Cmd.none )

        VisibilityChanged visibility ->
            ( { model | visibility = visibility }, Cmd.none )

        OptionButtonPressed button ->
            case button of
                "pause" ->
                    ( { model | playing = not model.playing }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        _ ->
            ( model, Cmd.none )


type Msg
    = KeyChanged Bool String
    | Tick Time.Posix
    | MouseMove Float Float
    | MouseClick
    | VisibilityChanged E.Visibility
    | OptionButtonPressed String


setKeyboard : List ( String, String ) -> Attribute.Attribute msg
setKeyboard mapping =
    MappedSetOfKeys (Set.fromList mapping)


keyToAction : String -> String -> ( String, String )
keyToAction from to =
    Tuple.pair from to


setScreen : Float -> Float -> Attribute.Attribute msg
setScreen width height =
    NewScreen (toScreen width height)


type NodeProperty
    = Id String
    | Description String
    | Requirement (List Achievement)
    | Expectation


setId : String -> NodeProperty
setId id_ =
    Id id_


show : Shape -> Ms -> Ms -> Node
show shape start end =
    schedulePicture shape start end
        |> flip newSequence emptyNode


flip =
    \a c b -> a b c



--show : Picture -> Ms -> Ms -> Node


require : List Achievement -> NodeProperty
require achievements =
    Requirement achievements


block : List NodeProperty -> List Node -> Node
block properties children =
    List.foldl
        (\x acc ->
            case x of
                Id str ->
                    newNodeId str acc

                Description str ->
                    newNodeDescription str acc

                Requirement ls ->
                    newNodeRequirement ls acc

                _ ->
                    acc
        )
        (Canopy.Node emptyData
            children
        )
        properties
