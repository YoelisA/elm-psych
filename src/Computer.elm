module Computer exposing (..)

import Set exposing (Set)
import Time exposing (Posix, millisToPosix)


type alias Computer =
    { mouse : Mouse
    , keyboard : Keyboard
    , screen : Screen
    , time : Time
    }


initComputer : Screen -> Computer
initComputer screen =
    { mouse =
        { position = ( 0, 0 )
        , isDown = False
        , isClick = False
        }
    , keyboard =
        emptyKeyboard
    , screen = screen
    , time = Time (millisToPosix 0)
    }


emptyKeyboard : Keyboard
emptyKeyboard =
    Keyboard
        False
        False
        False
        False
        False
        False
        False
        False
        Set.empty



-- Peut être contraidre l'update du clavier en fonction des attributs de l'environnement ?


newKeyboard : Keyboard -> Computer -> Computer
newKeyboard keyboard_ { mouse, keyboard, screen, time } =
    Computer mouse keyboard_ screen time


type alias IsDownMappedKey =
    { isDown : Bool
    , key : String
    , mappedSetOfKeys : Set ( String, String )
    }


updateComputerKeyboard keyboard =
    newKeyboard << newKeyEvent keyboard


newKeyEvent : IsDownMappedKey -> Keyboard -> Keyboard
newKeyEvent { isDown, key, mappedSetOfKeys } keyboard =
    let
        keys =
            if isDown then
                Set.insert key keyboard.keys

            else
                Set.remove key keyboard.keys
    in
    case Set.member key (Set.map Tuple.first mappedSetOfKeys) of
        True ->
            case key of
                " " ->
                    { keyboard | keys = keys, space = isDown }

                "Enter" ->
                    { keyboard | keys = keys, enter = isDown }

                "Shift" ->
                    { keyboard | keys = keys, shift = isDown }

                "Backspace" ->
                    { keyboard | keys = keys, backspace = isDown }

                "ArrowUp" ->
                    { keyboard | keys = keys, up = isDown }

                "ArrowDown" ->
                    { keyboard | keys = keys, down = isDown }

                "ArrowLeft" ->
                    { keyboard | keys = keys, left = isDown }

                "ArrowRight" ->
                    { keyboard | keys = keys, right = isDown }

                _ ->
                    { keyboard | keys = keys }

        False ->
            keyboard


type alias Keyboard =
    { up : Bool
    , down : Bool
    , left : Bool
    , right : Bool
    , space : Bool
    , enter : Bool
    , shift : Bool
    , backspace : Bool
    , keys : Set String
    }


type alias Mouse =
    { position : Position
    , isDown : Bool
    , isClick : Bool
    }


type alias Position =
    ( Float, Float )



type alias Screen =
    { width : Float
    , height : Float
    , top : Float
    , left : Float
    , right : Float
    , bottom : Float
    }


type Time
    = Time Posix


toScreen : Float -> Float -> Screen
toScreen width height =
    { width = width
    , height = height
    , top = height / 2
    , left = -width / 2
    , right = width / 2
    , bottom = -height / 2
    }
