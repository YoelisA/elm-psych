module Experiment exposing (..)

import Browser
{--
import Canopy exposing (..)
import Dict exposing (..)
import Id exposing (Id, from)
import Random exposing (..)
import Response exposing (Type_(..))
import Scene exposing (Scene)
import Set exposing (..)
import Stimulus exposing (Stimulus, stimulus)
import Time exposing (Posix)
import Trial exposing (Trial, trial)
import Unique exposing (..)


type Experiment = Experiment
--}
{--type alias ExperimentTree a =
    Node NodeType


rootb : ExperimentTree
rootb =
    node
        (Ex
            { id = Id ""
            , nodeType = "ex"
            , logic = Set.empty
            , memory = []
            , recipe = []
            }
        )
        [ node
            (Bl
                { id = Id ""
                , nodeType = "ex"
                , logic = Set.empty
                , memory = []
                , recipe = []
                }
            )
            []
        ]





block : List Trial -> ExperimentTree
block trials =
    node
        (Bl
            { id = Id ""
            , nodeType = "ex"
            , logic = Set.empty
            , memory = []
            , recipe = []
            }
        )
        (List.map (\trial_ -> leaf (Tr trial_)) trials)


trial : ExperimentTree
trial =
    leaf
        (Tr
            { id = Id ""
            , nodeType = "ex"
            , logic = Set.empty
            , memory = []
            , recipe = []
            }
        )


type NodeType
    = Ex Experiment
    | Bl Block
    | Tr Trial


type alias ExperimentNode =
    { id : Id
    , nodeType : String
    , logic : Set (Bool -> Logic)
    , recipe : List Event
    , memory :
        List
            { id : Id
            , tag : String
            , behaviour : Behaviour
            , timeStamp : Time.Posix
            , blockId : Id
            , subjectId : Id
            }
    }


type alias Block_ a =
    { a
        | id : Id
        , logic : Set (Bool -> Logic)
        , memory :
            List
                { id : Id
                , tag : String
                , behaviour : Behaviour
                , timeStamp : Time.Posix
                }
    }


type alias Trial_ a =
    { a
        | id : Id
        , recipe : List Event
    }


type alias Trial =
    Trial_ ExperimentNode


type alias Block =
    Block_ ExperimentNode


type alias Experiment =
    Experiment_ ExperimentNode


type alias Experiment_ a =
    { a
        | id : Id
        , memory :
            List
                { id : Id
                , tag : String
                , behaviour : Behaviour
                , timeStamp : Time.Posix
                }
    }


type Event
    = ResponseWindow (Set Tag) JitterWindowEvent KeyMapping
    | Picture (Set Tag) JitterWindowEvent
    | OneChoiceQuestion (Set Tag) JitterWindowEvent Question
    | MultipleChoice (Set Tag) JitterWindowEvent Question


type alias Tag =
    String


type alias Question =
    { question : String
    , answers : Set String
    , id : Id
    , order : Random.Generator (List Int)
    }


type alias PresentationOrder =
    Set Int


type alias JitterWindowEvent =
    ( Random.Generator ( Int, Int ), Random.Generator ( Int, Int ) )


type alias KeyMapping =
    Set ( String, Behaviour )


type Behaviour
    = KeyPressed KeyId


type alias KeyId =
    String


type Logic
    = Repeat
    | ToNext
    | ToPrev
    | ToId Id


type Id
    = Id String
--}
{--
type alias GenScore =
    List ( { start : ( Int, Int ), end : ( Int, Int ) }, Event )


type alias Score =
    List ( { start : ( Int, Int ), end : ( Int, Int ) }, Event )


type alias Memory =
    GenScore


type Type
    = Block
    | Trial
    | Exp


initExperiment : String -> Tree
initExperiment name =
    Node { name = name, type_ = Exp, score = Nothing, memory = [] } []


experiment : String -> List { name : String, nTrial : Int, score : Score } -> Tree
experiment experimentName data =
    experimentHelp experimentName data (initExperiment experimentName)


experimentHelp : String -> List { name : String, nTrial : Int, score : Score } -> Tree -> Tree
experimentHelp experimentName data treeSoFar =
    let
        root =
            node rootId []

        rootId =
            GenData experimentName Exp Nothing []

        blockId name =
            GenData name Block Nothing []

        seedTrial : Int -> String -> Score -> Tree -> Tree
        seedTrial counter blockId_ score myTree =
            case counter of
                0 ->
                    myTree

                _ ->
                    seedTrial
                        (counter - 1)
                        blockId_
                        score
                        (myTree
                            |> append (blockId blockId_)
                                { name = "Trial " ++ String.fromInt counter
                                , type_ = Trial
                                , score = Just score
                                , memory = []
                                }
                        )
    in
    case data of
        [] ->
            treeSoFar

        { name, nTrial, score } :: xs ->
            experimentHelp
                experimentName
                xs
                treeSoFar
                |> append rootId
                    { name = name
                    , type_ = Block
                    , score = Nothing
                    , memory = []
                    }
                |> seedTrial nTrial name score


type Expectation
    = Expected Event
    | Unexepected Event Event


type Event
    = TimeOut
    | ResponseWindow
    | String

type alias Data =
    { name : String
    , type_ : Type
    , score : Maybe Score
    , memory : Memory

    --, expectations : List Expectation
    --, score : List ( Int, Event )
    }


type alias GenData =
    { name : String
    , type_ : Type
    , score : Maybe GenScore
    , memory : Memory

    --, expectations : List Expectation
    --, score : List ( Int, Event )
    }
type alias Tree =
    Node GenData
type alias ActualTree =
        Node Data
type Experiment
    = Experiment Tree Scene



--}
a = 2
