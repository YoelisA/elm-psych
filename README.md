## Readme elm-psych 

## Intro

DSL -> Experiments in psychology on the web. It's not even a prototype. 

## Set-Up 

Todo: Set up 



## Documentation

Let's say you'd  like to run again the Stroop's Task. 

The Stroop's Task has been used in plenty of experiments' design and here's the plot: 

>  The presentation of incongruent pairs of word and color (e.g : "BLUE") will impair the subject's RT and increase the number of incorrect answer. 

Everything starts with an empty `environnement`. 

```elm
main = 
	environnement [][]
```

You give nothing, you get nothing. A pretty boring experiment but that's a start.

One can add some action in introducing a keyboard with some mapped keys along with a screen.

```elm
main =
	environnement 
		[ setKeyboard 
			[ map Key.arrowLeft Action.Prev
			, map (key arrowRight) (action Next) 
			]
		]
		[]
```

Now a lot of things can happen like printing pictures and get some keys events. 

It's time to introduce you to the main building block of an experiment : the Block. 

```elm

main =
	environnement 
		[ setKeyboard 
			[ keyToAction "ArrowLeft" Prev
			, keyToAction "ArrowRight" Next 
			]
		]
		[ block [][] ]
```

A `block` can contain other blocks so it forms a tree. 

This tree will help us shape our experiment. 

```elm

main =
	environnement 
		[ setKeyboard 
			[ keyToAction "ArrowLeft" ToPrev
			, keyToAction "ArrowRight" ToNext 
			]
		]
		[ block 
			[ description "We compare the reaction of the subject when shown two different types of stimulus"
			, shuffle all
			, require [ "human" ]
			]
			[ block 
				[ description "In this block we show incongruent stimuli"
                , repeat (times 10)
                , setKeyboard 
                	[ keyToAction custom "r" "Red"
                    , keyToAction custom "b" "Blue"
                    , keyToAction custom "g" "Green"
                    ]
                ]
				[ show (fixationCross) (ms 0) (ms 100) 
				, show (whiteScreen) (ms 150) (ms 450)
				, show 
					( pattern 
						[ setVariable "Blue"  [ red, green ]
						, setVariable "Green" [ blue, red ]
						, setVariable "Red"   [ green, blue ]
						]
						[ text variable ]
						) 
					(ms 450) 
					(ms 1200)
				, watch () (ms 450) (ms 5000)
				]
			, block 
				[ description "In this block we show congruent stimuli"
				, repeat (times 10)
				]
				[ show (fixationCross) (ms 0) (ms 100) 
				, show (whiteScreen) (ms 150) (ms 450)
				, show 
					( pattern 
						[ setVariable "Blue"  [ blue ]
						, setVariable "Green" [ green ]
						, setVariable "Red"   [ red ]
						]
						[ text variable ]
					) 
					(ms 450) 
					(ms 1200) 
				, watch () (ms 450) (ms 5000)
				]
			]
         , block 
         	 [ description "Ending block of the Stroop Task."
         	 ]
         	 [show 
         	 	(paragraph [text "Merci de votre participation !"]) 
         	 	(ms 0) 
         	 	(ms 10000)
         	 ]
			]
```

|      | SubjectId | BlockId | TrialId | Stimulus | StimulusTime | Action | ActionTime |      |      |
| ---- | --------- | ------- | ------- | -------- | ------------ | ------ | ---------- | ---- | ---- |
|      | Subj1     | Block1  | 03      |          |              |        |            |      |      |
|      | Subj1     | Block2  | 01      |          |              |        |            |      |      |
|      | Subj1     | Block1  | 05      |          |              |        |            |      |      |
|      | Subj1     | Block1  | 07      |          |              |        |            |      |      |
|      | Subj1     | Block1  | 08      |          |              |        |            |      |      |
|      | Subj1     | Block2  | 04      |          |              |        |            |      |      |
|      | Subj1     | Block2  | 02      |          |              |        |            |      |      |
|      | Subj1     | Block2  | 07      |          |              |        |            |      |      |
|      |           |         |         |          |              |        |            |      |      |

